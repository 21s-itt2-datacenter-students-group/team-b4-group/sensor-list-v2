**Sensor list that we are using:**
   | sensor name   | cost  | pros  | cons   | link  |
|---|---|---|---|---|
| DHT11 | kr33.39 | cheap, has 2 sensors in it | not accurate | https://dk.farnell.com/en-DK/dfrobot/dfr0067/temperature-humidity-sensor-arduino/dp/2946103?st=dht11|
| Sensirion SDP810-125 PA | kr. 276.62 | can meassure airflow and have digital pins | more expensive | https://dk.farnell.com/en-DK/sensirion/sdp810-125pa/pressure-sensor-digital-125pa/dp/2886665?ost=sdp810-125+pa&cfm=true |
|Barometer/temerature sensor/altitude sensor| 99 DKK not incl. shipping |High precision | | https://www.elextra.dk/p/GS/H32005 |
|dust sensor|114 DKK|quality sensor||  https://let-elektronik.dk/shop/1500-biometri--gas/9689-optical-dust-sensor---gp2y1010au0f/ |
